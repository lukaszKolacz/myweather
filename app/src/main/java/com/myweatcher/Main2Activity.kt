package com.myweatcher

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_main2.*

class Main2Activity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main2)

        if(intent.extras != null){
            val customText:String = intent.extras.getString("new_text", "nic tu ni ma")
            tvMain.text = customText
        }
    }

//    override fun onBackPressed() {          //co ma robić po naciśnięciu back
//        super.onBackPressed()
////        finish()                         //Robi to samo co: super.onBackPressed()
//    }

}
