package com.myweatcher

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        btnGoNextActivity.setOnClickListener {
            //            Toast.makeText(this, "Przycisk dziala", Toast.LENGTH_SHORT).show()
            val intent = Intent(this, Main2Activity::class.java)
            intent.putExtra("new_text", "Text by intent")
            this.startActivity(intent)
//            this.sendBroadcast(intent)
        }
    }
}
